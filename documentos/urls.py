from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^documento/set/insert/$', DocumentoCreateAPIView.as_view(), name="insert_country"),
    url(r'^documento/set/update/(?P<pk>[0-9]+)/$', DocumentoUpdateAPIView.as_view(), name="update_country"),
    url(r'^documento/set/delete/(?P<pk>[0-9]+)/$', DocumentoUpdateAPIView.as_view(), name="update_country"),
    url(r'^documento/set/active/(?P<pk>[0-9]+)/$', DocumentoActiveAPIView.as_view(), name="update_country"),
    url(r'^documento/get/select/$', DocumentoCreateAPIView.as_view(), name="update_country"),
    url(r'^documento/get/select/(?P<pk>[0-9]+)/$', DocumentoUpdateAPIView.as_view(), name="selectpk_Documento"),
    url(r'^evaluacion/set/insert/$', EvaluacionCreateAPIView.as_view(), name="insert_evaluacion"),
    url(r'^evaluacion/set/update/(?P<pk>[0-9]+)/$', EvaluacionUpdateAPIView.as_view(), name="update_evaluacion"),
    url(r'^evaluacion/set/delete/(?P<pk>[0-9]+)/$', EvaluacionUpdateAPIView.as_view(), name="update_evaluacion"),
    ##url(r'^evaluacion/set/active/(?P<pk>[0-9]+)/$', EvaluacionActiveAPIView.as_view(), name="set_evaluacion"),
    url(r'^evaluacion/get/select/$', EvaluacionCreateAPIView.as_view(), name="update_evaluacion"),
    url(r'^evaluacion/get/select/(?P<pk>[0-9]+)/$', EvaluacionUpdateAPIView.as_view(), name="detail_evaluacion"),
]