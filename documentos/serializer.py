from rest_framework import serializers
from .models import *


class DocumentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Documento
        fields = '__all__'

class DocumentoActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Documento
        fields = ("estado","fecha_modificacion",)

class EvaluacionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Evaluacion
		fiedls = '__all__'