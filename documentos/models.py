from django.db import models

# Create your models here.

class Documento(models.Model):
    codigo = models.CharField(max_length=10,unique=True)
    titulo = models.CharField(max_length=50, blank=True, null=True)
    direccion = models.CharField(max_length=100,blank=True,null=True)
    autor = models.CharField(max_length=50,blank=True,null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True) 
    fecha_modificacion = models.DateTimeField(auto_now=True)
    estado = models.CharField(max_length=1,blank=True, null=True)

class Evaluacion(models.Model):
    codigo = models.CharField(max_length=10, unique=True, null=True)
    nombre = models.CharField(max_length=200, blank=True)
    responsable = models.CharField(max_length=60, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    estado = models.CharField(max_length=1,blank=True, null=True)