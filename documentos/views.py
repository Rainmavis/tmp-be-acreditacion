from django.shortcuts import render
from .models import *
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import *
import json
# Create your views here.
class DocumentoCreateAPIView(ListCreateAPIView):
	serializer_class = DocumentoSerializer
	def get_queryset(self):
		return Documento.objects.all().order_by('fecha_creacion')

class DocumentoUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = DocumentoSerializer
	queryset = Documento.objects.all().order_by('fecha_creacion')

class DocumentoActiveAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = DocumentoUpdateAPIView
	queryset = Documento.objects.all().order_by('fecha_creacion')

class EvaluacionCreateAPIView(ListCreateAPIView):
	serializer_class = EvaluacionSerializer
	def get_queryset(self):
		return Evaluacion.objects.all().order_by('nombre')

class EvaluacionUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = EvaluacionSerializer
	queryset = Evaluacion.objects.all().order_by('nombre')
